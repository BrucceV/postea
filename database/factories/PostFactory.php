<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $users = User::all();
    $user_ids = array();
    foreach($users as $user)
    {
        $user_ids[] = $user->id;
    }
    return [
      'title' => substr($faker->sentence(2), 0, -1),
      'img' => $faker->image('public/images', 400, 300, null, false),
      'tags' => $faker->words($nb = 3, $asText = False)
         //
    ];
});
